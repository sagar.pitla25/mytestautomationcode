﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAutomation.Page_Objects;
namespace TestAutomation
{
    
    public class Login
    {

        IWebDriver driver;
        
          
        [SetUp]
        public void start()
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("disable-infobars");
            driver = new ChromeDriver();
            driver.Url = "http://www.google.com/";
            driver.Manage().Window.Maximize();
        }
        [Test]
        public void MainScreen()
        {
            
            PageObject pg = new PageObject(driver);
            pg.data1();
            
        }

        [TearDown]
        public void end()
        {
            driver.Quit();
        }

    }
}
